from rest_framework import serializers
from rest_framework_gis import serializers as gis_serializers
from api.models import Tree, TreeChange, Tag, Key
from django.core.exceptions import ObjectDoesNotExist


class KeySerializer(serializers.ModelSerializer):
    class Meta:
        model = Key
        fields = ["tag_type", "key"]


class TagSerializer(serializers.ModelSerializer):
    key = KeySerializer(read_only=False)

    def create(self, validated_data):
        key_data = validated_data.pop("key")
        key = Key.objects.get(key=key_data.get("key"))
        tag = Tag.objects.create(key=key, **validated_data)
        return tag

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        tag_type = ret["key"]["tag_type"]
        value = ret["value"]
        if tag_type == "text":
            ret["value"] = ret["value"]
        if tag_type == "numeric":
            ret["value"] = float(ret["value"])

        if tag_type == "boolean":
            ret["value"] = bool(ret["value"])
        return ret

    class Meta:
        model = Tag
        fields = ["key", "value"]


class TreeChangeSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = TreeChange
        fields = ["id", "tree", "edited_at", "tags"]

    def create(self, validated_data):
        tags_data = validated_data.pop("tags")
        datapoint = TreeChange.objects.create(**validated_data)
        for tag_data in tags_data:
            tag_ser = TagSerializer(data=tag_data)
            if tag_ser.is_valid():
                tag = tag_ser.create(tag_ser.validated_data)
                datapoint.tags.add(tag)

        return datapoint


class TreeSerializer(gis_serializers.GeoFeatureModelSerializer):
    changes = TreeChangeSerializer(many=True)

    def create(self, validated_data):
        datapoints_data = validated_data.pop("changes")
        tree = Tree.objects.create(**validated_data)
        for datapoint_data in datapoints_data:
            dp_ser = TreeChangeSerializer(data=datapoint_data)
            if dp_ser.is_valid():
                datapoint = dp_ser.create(dp_ser.validated_data)
                tree.changes.add(datapoint)
        return tree

    class Meta:
        model = Tree
        geo_field = "geom"
        precision = 5
        fields = [
            "id",
            "geom",
            "name",
            "species",
            "changes",
        ]
