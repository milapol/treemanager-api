from django.contrib.gis.db import models
from django.db.models import JSONField
from django.utils import timezone

# Create your models here.
TAG_TYPES = (
    ("text", "Text"),
    ("boolean", "Bool"),
    ("numeric", "Numeric"),
)


class Node(models.Model):
    # TODO: created_by = models.ForeignKey(auth, on_delete)
    geom = models.PointField()

    @property
    def latlon(self):
        return [self.geom[0], self.geom[1]]

    class Meta:
        abstract = True


class Tree(Node):
    name = models.CharField(max_length=5)
    species = models.CharField(max_length=42)

    def __str__(self):
        return "Tree %s at: [%s, %s]" % (
            self.name,
            str(self.latlon[0])[:7],
            str(self.latlon[1])[:7],
        )


class TreeChange(models.Model):
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, related_name="changes", null=True)
    edited_at = models.DateField()

    class Meta:
        ordering = ["tree", "edited_at"]

    def __str__(self):
        return "%s, %s" % (str(self.tree.name), str(self.edited_at))


class Key(models.Model):
    tag_type = models.CharField(choices=TAG_TYPES, max_length=10)
    key = models.CharField(max_length=200)

    def __str__(self):
        return self.key


class Tag(models.Model):
    tree_change = models.ForeignKey(TreeChange, on_delete=models.CASCADE, null=True, related_name="tags")
    key = models.ForeignKey(Key, on_delete=models.SET_NULL, null=True, related_name="values")
    value = models.CharField(max_length=200)

    def __str__(self):
        return "%s : %s" % (self.key.key, str(self.value))
