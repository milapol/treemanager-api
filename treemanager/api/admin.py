from django.contrib.gis import admin
from django.contrib.gis.admin import GISModelAdmin, GeoModelAdmin
from api.models import Tree, TreeChange, Tag, Key

admin.site.register(Tree, GISModelAdmin)
admin.site.register(TreeChange)
admin.site.register(Tag)
admin.site.register(Key)
