from django.urls import include, path
from api.views import TreeList, TreeDetail, TreeCreate, TreeChangeDetail, TreeChangeList, TreeChangeCreate, KeyList
from api.serializers import TreeSerializer, TreeChangeSerializer, KeySerializer

urlpatterns = [
    path("trees/", TreeList.as_view(serializer_class=TreeSerializer), name="api-list-trees"),
    path("trees/create/", TreeCreate.as_view(serializer_class=TreeSerializer), name="api-create-tree"),
    path("tree/<int:pk>", TreeDetail().as_view(serializer_class=TreeSerializer), name="api-detail-tree"),
    path("datapoint/<int:pk>", TreeChangeDetail(serializer_class=TreeChangeSerializer).as_view(), name="api-detail-tree-change"),
    path("datapoints/", TreeChangeList(serializer_class=TreeChangeSerializer).as_view(), name="api-list-tree-change"),
    path("datapoints/create/", TreeChangeCreate(serializer_class=TreeChangeSerializer).as_view(), name="api-list-tree-change"),
    path("keys/", KeyList.as_view(serializer_class=KeySerializer), name="api-list-keys"),
]
