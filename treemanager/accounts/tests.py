from django.test import TestCase
from django.contrib.auth.models import Group
from accounts.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user
from accounts.models import User


class TestUserModel(TestCase):
    def setUp(self):
        self.test_username = "testuser"
        self.test_password = "FortyTwo42"

    def test_user_without_username(self):
        """
        Test create_user without username
        """
        try:
            User.objects.create_user(username=None, password=self.test_password),
        except ValueError:
            assert True

    def test_create_superuser_credentials(self):
        """
        Test create_superuser credentials
        """
        user = User.objects.create_superuser(
            username=self.test_username, password=self.test_password
        )
        self.assertTrue(user.check_password(self.test_password))
        self.assertEquals(user.username, "testuser")

    def test_create_superuser_authorization(self):
        """
        Test create_superuser authorization
        """
        user = User.objects.create_superuser(
            username=self.test_username, password=self.test_password
        )
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_active)

    def test_create_user_creation(self):
        """
        Test create_user creation
        """
        user = User.objects.create_user(
            username=self.test_username, password=self.test_password
        )
        self.assertTrue(user.check_password(self.test_password))
        self.assertEquals(user.username, self.test_username)

    def test_create_user_credentials(self):
        """
        Test create_user credentials
        """
        user = User.objects.create_user(
            username=self.test_username, password=self.test_password
        )
        self.assertTrue(user.check_password(self.test_password))
        self.assertEquals(user.username, self.test_username)

    def test_create_user_authorization(self):
        """
        Test create_user authorization
        """
        user = User.objects.create_user(
            username=self.test_username, password=self.test_password
        )
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertTrue(user.is_active)


class TestLoginView(TestCase):
    def setUp(self):
        self.test_username = "testuser"
        self.test_password = "FortyTwo42"
        user = User.objects.create(username=self.test_username)
        user.set_password(self.test_password)
        user.save()
        self.client = Client()

    def test_accounts_login_get(self):
        """
        Test login get
        """
        response = self.client.get(reverse("login"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "login.html")

    def test_accounts_login_valid(self):
        """
        Test login valid
        """
        username = self.test_username
        password = self.test_password
        response = self.client.post(
            reverse("login"),
            {"username": username, "password": password},
            follow=True,
        )
        self.assertRedirects(response, reverse("map"))
        self.assertTrue(response.context["user"].is_authenticated)
        self.assertTrue(response.context["user"].is_active)
        self.assertFalse(response.context["user"].is_staff)
        self.assertFalse(response.context["user"].is_superuser)
        self.assertEquals(response.context["user"].username, username)

    def test_accounts_login_post_invalid_login_form_username(self):
        """
        Test invalid login form username
        """
        response = self.client.post(
            reverse("login"), {"username": "test", "password": self.test_password}
        )
        self.assertFalse(response.context["user"].is_authenticated)
        self.assertInHTML("Invalid login credentials", str(response.content))

    def test_accounts_login_post_invalid_login_form_password(self):
        """
        Test invalid login form password
        """
        response = self.client.post(
            reverse("login"),
            {"username": self.test_username, "password": "LolLol12345"},
        )
        self.assertFalse(response.context["user"].is_authenticated)
        self.assertInHTML("Invalid login credentials", str(response.content))


class TestRegisterViews(TestCase):
    def setUp(self):
        self.test_username = "testuser"
        self.test_username2 = "testuser2"
        self.test_password = "FortyTwo42"
        user = User.objects.create(username=self.test_username)
        user.set_password(self.test_password)
        user.save()
        self.client = Client()

    def test_accounts_register_get(self):
        """Test register already"""
        response = self.client.get(reverse("register"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "register.html")

    def test_accounts_register_post_passwords_do_not_match(self):
        """Test register not matching passwords"""
        response = self.client.post(
            reverse("register"),
            {
                "username": self.test_username2,
                "password1": self.test_password,
                "password2": "FortyThree43",
            },
            follow=True,
        )
        self.assertEquals(len(User.objects.filter(username=self.test_username2)), 0)
        self.assertInHTML("Passwords do not match", str(response.content))

    def test_accounts_register_post_user_already_exists(self):
        """Test register already existing user"""
        response = self.client.post(
            reverse("register"),
            {
                "username": self.test_username,
                "password1": self.test_password,
                "password2": self.test_password,
            },
            follow=True,
        )
        self.assertInHTML("Username is already in use", str(response.content))
        self.assertEquals(len(User.objects.filter(username=self.test_username)), 1)

    def test_accounts_register_post_user_weak_password(self):
        """Test register weak Password"""
        response = self.client.post(
            reverse("register"),
            {
                "username": self.test_username,
                "password1": "42",
                "password2": "42",
            },
            follow=True,
        )
        self.assertInHTML("Password is to weak", str(response.content))

    def test_accounts_register_post(self):
        """Test register valid"""
        response = self.client.post(
            reverse("register"),
            {
                "username": self.test_username2,
                "password1": self.test_password,
                "password2": self.test_password,
            },
            follow=True,
        )
        self.assertEquals(len(User.objects.filter(username=self.test_username2)), 1)
        self.assertEquals(
            User.objects.get(username=self.test_username2), response.context["user"]
        )


class TestLogoutViews(TestCase):
    """Test logout valid"""
    def setUp(self):
        self.test_username = "testuser"
        self.test_password = "FortyTwo42"
        user = User.objects.create(username=self.test_username)
        user.set_password(self.test_password)
        user.save()
        self.client = Client()

    def test_accounts_logout_get(self):
        self.client.login(username=self.test_username, password=self.test_password)
        response = self.client.get(reverse("map"))
        self.assertTrue(response.context["user"].is_authenticated)
        self.assertInHTML("Logout", str(response.content))

        response = self.client.get(reverse("logout"), follow=True)
        self.assertRedirects(response, reverse("map"))
        self.assertFalse(response.context["user"].is_authenticated)
        self.assertInHTML("Login", str(response.content))
