from django.urls import path
from accounts.views import LoginView, UserApi

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("me/", UserApi.as_view(), name="me"),
]
