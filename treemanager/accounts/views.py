from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response

from accounts.serializers import LoginSerializer, UserSerializer


class LoginView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        serializer = LoginSerializer(data=self.request.data,
                                     context={'request': self.request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return Response({}, status=status.HTTP_202_ACCEPTED)


class UserApi(generics.GenericAPIView):
    def get(self, request):
        user_ser = UserSerializer(request.user)
        return Response(
            {
                "authenticated": True,
                "user": user_ser.data,
            }
        )
